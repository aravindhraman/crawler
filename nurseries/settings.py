# -*- coding: utf-8 -*-

# Scrapy settings for nurseries project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'nurseries'

SPIDER_MODULES = ['nurseries.spiders']
NEWSPIDER_MODULE = 'nurseries.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'nurseries (+http://www.yourdomain.com)'

HTTPCACHE_ENABLED = True
HTTPCACHE_EXPIRATION_SECS = 0
HTTPCACHE_DIR = "/home/aravindh/EK/crawler/nurseries/nurseries/data"