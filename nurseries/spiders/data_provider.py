'''Ofsted crawler
pass the final loaction to download 

'''

from scrapy.spider import Spider
import math, os
from scrapy.http import Request

from nurseries.items import OfstedNurseriesItem, OtherOfstedItem, DayNurseriesItem

OFSTED_PROVIDER_TYPES = {
    46 : "16-19 academy",
    8 : "Ad Support Agencies",
    42 : "Adult and Community Learning",
    1 : "Boarding School",
    12 : "Cafcass",
    15 : "Childcare on Domestic Premises",
    16 : "Childcare on Non-Domestic Premises",
    17 : "Childminder",
    1000 : "Childminder Agency",
    2 : "Children's Home",
    45 : "Children's centre groups",
    13 : "Children's centres",
    29 : "DWP Provisions including New Deal and Workstep",
    38 : "Dance and Drama Schools and Colleges",
    30 : "Employer Provider",
    3 : "FE College with Residential Care",
    31 : "General Further Education and Tertiary",
    18 : "Home Childcarer",
    35 : "IAG/Next Step",
    32 : "Immigration removal centres",
    4 : "Independent Fostering Services",
    33 : "Independent Learning Provider",
    27 : "Independent School",
    39 : "Independent Specialist College",
    41 : "Initial Teacher Education",
    34 : "Learn Direct",
    44 : "Local Authority",
    6 : "Local Authority Ad Agency",
    5 : "Local Authority Fostering",
    20 : "Nursery",
    21 : "Primary",
    36 : "Prison and young offender institutions",
    37 : "Probation Trusts",
    22 : "Pupil Referral Unit",
    9 : "Residential Family Centre",
    10 : "Residential Special School",
    23 : "Secondary",
    11 : "Secure Training Centres",
    26 : "Service children's education",
    40 : "Sixth Form College",
    24 : "Sixth Form School",
    25 : "Special School",
    7 : "Voluntary Ad Agency",
    14 : "Other children and families services",
    19 : "Other early years and childcare",
    43 : "Other further education and skills",
    28 : "Other schools"
}

PROVIDER_STATUS = {
    0: 'operating',
    1: 'all'
}


class OfstedSpider(Spider):
    name = "ofsted"
    allowed_domains = ["reports.ofsted.gov.uk"]
    BASE_URL = "http://reports.ofsted.gov.uk"

    def __init__(self, provider_type, file_location = None, *args, **kwargs):
        super(OfstedSpider, self).__init__(*args, **kwargs)
        self.provider_type = provider_type
        print type( self.provider_type ), self.provider_type
        if type( self.provider_type ) != int:
            print next((p_id for p_id, p_name in OFSTED_PROVIDER_TYPES.items() if p_name == provider_type), None)
            self.provider_type = next((p_id for p_id, p_name in OFSTED_PROVIDER_TYPES.items() if p_name == provider_type), None)
        
        if file_location:
            self.file_location = file_location

        #TODO: Parse the page numbers from the page itself
        self.start_urls = self.set_start_urls( 1, 2 )
        

    def set_start_urls( self, from_page, to_page  ):
        
        self.operating_status = next((key for key, status in PROVIDER_STATUS.items() if status == 'all'),None)       
        self.from_page = from_page - 1
        self.to_page = to_page - 1

        start_urls = [
            "http://reports.ofsted.gov.uk/inspection-reports/find-inspection-report/results/any/%d/any/any/any/any/any/any/any/any/0/0?page=%d&sort=%d" % (self.provider_type, self.operating_status, page) for page in xrange(self.from_page, self.to_page)
        ]

        return start_urls

    def parse( self, response ):
        requests = []
        page_links = response.xpath('//div[@id="content"]/ul/li/h2/a/@href').extract()
        for page_link in page_links:
            url = self.BASE_URL + page_link
            requests.append(Request( url = url, callback = self.parse_nursery ))
        return requests

    def parse_nursery( self, response ):
        business_name = response.xpath('//div[@id="content"]/h1/text()').extract()[0]
        urn = response.xpath('//div[@id="inspection-reports-main"]/p[@class="urn-number"]/strong/text()').extract()[0]
        if urn:
            # overall_effectiveness = response.xpath('//div[@id="overall-effectivness-clone"]/span[@class="ins-judgement ins-judgement-2"]/text()').extract()[0]
            u_address = response.xpath('//div[@id="inspection-reports-main"]/p[2]/text()').extract()
            zip_code = u_address[-1].strip()
            u_address.pop(-1)
            city = u_address[-1].strip()
            u_address.pop(-1)
            address = ",".join( u_address ).strip()
            u_telephone = response.xpath('//div[@id="inspection-reports-main"]/p[3]/text()').extract()
            telephone = u_telephone[0].split(":")[1].strip()
            u_reports = response.xpath('//div[@id="archive-reports"]/table[@class="sticky-enabled sticky-table"]/tbody/tr/td/a/@href').extract()
            reports = ",".join( u_reports )
            ofsted_attrs  = OfstedNurseriesItem()
            ofsted_attrs['business_name'] = business_name
            ofsted_attrs['location_name'] = city
            ofsted_attrs['ofsted_urn'] = urn
            ofsted_attrs['address'] = address
            ofsted_attrs['zip_code'] = zip_code
            ofsted_attrs['city'] = city
            # ofsted_attrs.country_id
            ofsted_attrs['contact_tel_1'] = telephone


            u_keys = response.xpath('//div[@id="provider-details"]/div/ul/li/strong/text()').extract()
            u_values = response.xpath('//div[@id="provider-details"]/div/ul/li/text()').extract()
            keys = [ key.lower().replace(" ","_").replace(":","") for key in u_keys ]
            values = [ value.strip() for value in u_values ]
            provider_details = dict(zip(keys, values))
            if provider_details:
                other_ofsted_attrs = OtherOfstedItem()
                #TODO: check whether the item is present in items.py
                for det in provider_details.items():
                    other_ofsted_attrs[ det[0] ] = det[1]               

            if other_ofsted_attrs['local_authority']:
                ofsted_attrs['location_name'] = other_ofsted_attrs['local_authority']

            return [ ofsted_attrs, other_ofsted_attrs ]

        else:
            print "IDs not matching", business_name

        return None, None


import ast
from datetime import time

class DayNurserySpider( Spider):
    name = "day_nursery"
    # start_urls = [ 'http://app.daynurseries.co.uk/getjson/?screen=totals_android' ]
    BASE_URL = 'http://app.daynurseries.co.uk/'
    item = {}

    def __init__(self, item_from, item_to, *args, **kwargs):
        super(DayNurserySpider, self).__init__(*args, **kwargs)
        self.items_offset = 50
        # TODO: doing additional checks with from and to
       # if not item_from and not item_to:
            
       #      item_from = 1
       #      url = 'http://app.daynurseries.co.uk/getjson/?screen=totals_android'
       #      request = Request( url = url, callback = self.parse_total_items) 
       #      request.meta['item'] = self.item
       #      print request.meta  
       #      item_to = "22s"
       
        self.start_urls = self.set_start_urls( item_from, item_to )


    # def parse_total_items(self, response):
    #     print "in totals"
    #     totals = ast.literal_eval( response.xpath('//body/p/text()').extract()[0] )
    #     total_items = ( item['total'] for item in totals if item['actualname'] == "Day Nurseries" ).next()
    #     obj = response.meta['item']
    #     obj['item_to'] = int(total_items)
    #     print obj
    #     return obj

   
    def set_start_urls( self, from_item, to_item  ):
        start_urls = [
            self.BASE_URL + "getjson/?screen=list&cat=5000&startrow=%d&endrow=%d&long=-0.922852&lat=51.971346" % (start, (start - 1) + self.items_offset) for  start in range( int(from_item), int(to_item), self.items_offset )
        ]

        return start_urls


    def parse( self, response ):
        requests = []
        nurseries_list = ast.literal_eval( response.xpath('//body/p/text()').extract()[0] )
        for nursery in nurseries_list:
            requests.append(Request( url = self.BASE_URL + "getjson/?id=%d" % int(nursery['id']), callback =  self.parse_nursery))
        return requests

    def parse_nursery( self, response ):
        day_nursery = DayNurseriesItem()
        nursery_details = response.xpath('//body/p/text()').extract()
        nursery = ast.literal_eval( nursery_details[0] + nursery_details[1] )[0]
        day_nursery[ 'dn_id' ] = nursery[ 'id' ]
        day_nursery[ 'business_name' ] = nursery[ 'entryname' ]
        day_nursery[ 'image_urls' ] = [value for key, value in nursery.iteritems() if key.startswith("photourl") and value]
        day_nursery[ 'latitude' ] = nursery[ 'latitude' ]
        day_nursery[ 'longitude' ] = nursery ['longitude' ]
        website = nursery.get( 'website', None )
        if website :
            website = self.assign_website( website, day_nursery )
        else:
            day_nursery[ 'website_url' ] = "NA"
        address = nursery ['address' ].split(",")
        day_nursery[ 'country' ] = address.pop( -1 ).strip()
        day_nursery[ 'city' ] = address.pop( -1 ).strip()
        day_nursery[ 'address' ] = ",".join( address )
        day_nursery[ 'zip_code' ] = response.xpath('//span/p/span/text()').extract()[0].split(",")[-2].strip()
        day_nursery[ 'phone' ] =  Request( url = nursery[ 'telephone' ], callback =  self.parse_phone)
        
        u_keys = response.xpath('//span/strong/text()').extract()
        u_values = response.xpath('//span/text()').extract()
        keys = [ key.lower().split("/")[0].replace(":","").strip().replace(" ","_") for key in u_keys if key.strip() != "Admission Information:" ]
        values = [ value.strip() for value in u_values if value.strip() ]
        keys.reverse()
        values.reverse()

        dn_det = dict(zip( keys, values ))
        day_nursery[ 'person_in_charge' ] = dn_det.get( 'person_in_charge' , "NA")
        day_nursery[ 'service_offered' ] = dn_det[ 'service_offered' ]
        day_nursery[ 'total_capacity' ] = dn_det[ 'total_places_for_children' ]
        day_nursery[ 'holidays' ] = dn_det.get('when_closed' , "NA")
        day_nursery['local_authority'] = dn_det['local_authority']
        u_time = dn_det[ 'opening_hours' ].split("-")
        u_open_time = u_time[0]
        open_hr = int(u_open_time[0:2])
        open_min = int(u_open_time[2:4])
        u_close_time = u_time[1]
        close_hr = int(u_close_time[0:2])
        close_min =  int(u_close_time[2:4])
        day_nursery[ 'open_time' ] = time(open_hr, open_min)
        day_nursery[ 'close_time' ] = time(close_hr, close_min)
        day_nursery[ 'working_days' ] =  dn_det[ 'opening_days' ]
        u_age_range = dn_det[ 'age_range' ]
        age_range = [ int( u_age ) for u_age in u_age_range.split() if u_age.isdigit() ]
        if len( age_range ) == 4:
            day_nursery[ 'from_age' ] = age_range[ 0 ] * 12 + age_range[ 1 ]
            day_nursery[ 'to_age' ] = age_range[ 2 ] *12 + age_range[ 3 ]
        day_nursery[ 'no_of_staff' ] = dn_det.get( 'number_of_other_staff', "NA" )

        return day_nursery

    def assign_website( self, website, day_nursery  ):
        print "assign_website"
        request = Request( url=website,  meta = { 'object': day_nursery }, callback =  self.parse_url ) 
        yield request

    def parse_url( self, response ):
        redirected_resp = response.xpath('//body/script[2]/text()').extract()[0]
        u_redirect_url = redirected_resp.split(";")[0].strip()
        obj = response.meta['object']
        print "parse_url"
        print obj
        redirect_url = u_redirect_url [ u_redirect_url.index("(") + 2 : u_redirect_url.rindex("')") ]
        print redirect_url
        obj[ 'website_url' ] = redirect_url
        return obj

    def parse_phone( self, response ):
        phone_nos = response.xpath('//p[@align="left"]/em/text()').extract()
        if phone_nos:
            return phone_nos[0]
        return "NA"

