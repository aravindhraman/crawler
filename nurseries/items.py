# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy




class OfstedNurseriesItem(scrapy.Item):
    business_name = scrapy.Field()
    # url = scrapy.Field() 
    location_name = scrapy.Field() 
    ofsted_urn = scrapy.Field() 
    address = scrapy.Field() 
    zip_code = scrapy.Field() 
    city = scrapy.Field() 
    country_id = scrapy.Field() 
    contact_tel_1 = scrapy.Field() 
    # contact_email_1 = scrapy.Field() 
    # contact_email_2 = scrapy.Field() 
    # head_office_tel = scrapy.Field() 
    # head_office_email = scrapy.Field() 
    # description_of_services = scrapy.Field() 
    # take_debit_card_payments = scrapy.Field() 
    # max_fees = scrapy.Field() 
    # unit_of_fees = scrapy.Field() 
    # cost_unit = scrapy.Field() 

class OtherOfstedItem(scrapy.Item):
    age_range = scrapy.Field() 
    gender = scrapy.Field() 
    number_of_pupils_on_roll = scrapy.Field() 
    phase = scrapy.Field() 
    boarding_provision = scrapy.Field() 
    headteacher = scrapy.Field() 
    local_authority = scrapy.Field() 
    parliamentary_constituency = scrapy.Field() 
    type_of_establishment = scrapy.Field()


class DayNurseriesItem(scrapy.Item):
    dn_id = scrapy.Field()
    business_name = scrapy.Field()
    image_urls = scrapy.Field()
    open_time = scrapy.Field()
    close_time = scrapy.Field()
    working_days = scrapy.Field()
    holidays = scrapy.Field()
    from_age = scrapy.Field()
    to_age = scrapy.Field()
    person_in_charge = scrapy.Field()
    service_offered = scrapy.Field()
    total_capacity = scrapy.Field()
    no_of_staff = scrapy.Field()
    website_url = scrapy.Field()
    phone = scrapy.Field()
    latitude = scrapy.Field()
    longitude = scrapy.Field()
    local_authority = scrapy.Field()
    location_name = scrapy.Field() 
    address = scrapy.Field() 
    city = scrapy.Field() 
    country = scrapy.Field() 
    zip_code = scrapy.Field()
